// Remember, we're gonna use strict mode in all scripts now!
'use strict';

// console.log("boo");

// function squareDigits(num){
//     return (num*num);
// }

// console.log(`${squareDigits(9)}${squareDigits(1)}${squareDigits(1)}${squareDigits(9)}`);



function squareDigits(num){
    return Number(("" + num).split("").map (function (val) {
        return val * val
    }).join(''));
}

console.log(`${squareDigits(9119)}`);

// .split takes a string and splits it into an array based on the character(s) passed to it '' in this case.

// So

// ("9119").split('') === ["9", "1", "1", "9"]
// .map works like a for loop but takes a function as an argument. That function is applied to every member of the array.

// So

// ["9", "1", "1", "9"].map(function(val) { return val * val;}) === ["81", "1", "1", "81"]
// .join does the opposite of .split. It takes an Array and concatenates it into a string based on the character(s) passed to it.

// So

// ["81", "1", "1", "81"].join('') === "811181"



var x = [1, 2, 3, "a", "b"];

function filter_list(l) {
    // Return a new array with the strings filtered out
    
    l.filter (function (i) {
        if (typeof(i) !== "string");
        return i;
    });
    return l;
}
  
console.log(filter_list(x));












function letterCount(str) { 
    var letterList = 'A';
    var lCount = 0;
  
    for(var x = 0; x < str.length ; x++) {
        if (letterList.indexOf(str[x]) !== -1) {
        lCount += 1;
        }
  
    }
  return lCount;
}

var paragraph = document.getElementById("oneLetter").innerHTML;
console.log(letterCount(paragraph));
//console.log(letterCount(document.getElementById("oneLetter").innerHTML));


var contentCounter = document.createTextNode(" " + letterCount(paragraph));
document.getElementById("counter").appendChild(contentCounter);